import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

import Component from './components/Component';

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche la liste des pizzas
Router.menuElement = document.querySelector('.mainMenu');

Router.navigate();

const logo = document.querySelector('.logo');
logo.innerHTML += "<small>Les pizzas c'est la vie</small>";

const newsContainer = document.querySelector('.newsContainer');
newsContainer.setAttribute('style', 'display: block;');

/* Exemple pou recuperer des trucs dans des balises
const form = document.querySelector('form'), // balise form
	input = form.querySelector('input[name=message]'); // balise <input name="message">
*/
const closeButton = document.querySelector('.closeButton');
closeButton.addEventListener('click', () => {
	newsContainer.style = 'display: none;';
});
